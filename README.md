# سلام
من [عرفان][gitHub] هستم و میخوام تو این پروژه **مارک داون** رو تست کنم  :smile:

بنده کار آموز شرکت [**تابان شهر**][taban-e shahr] هستم  
و این لیستی از سر فصل های یاد گرفته شده هست  

* ~~امضای دیجیتال~~
* لینوکس
    * ~~شروع کار با ترمینال~~
    * ~~سطوح دسترسی~~
    * ~~مدیریت نرم افزار~~
        * ~~نصب node js و php~~
* ~~Git~~

* و همکنون در حال یادگیری مارک داون هستم  
 دیگر مبحث های مارک داون:    

##### عکس:  
`![alt text][image tag]`  
`[image tag]: "link Address"`  

##### Blockquote:  
`> "text or paragraph"`
##### تسک لیست:
- [] task 1
- [x] task 2

##### جدول:

عنوان 1 | عنوان 2
-------- | ---------
محتوای سلول 1 | محتوای سلول 2
محتوای ردیف اول | محتوای ردیف دوم

##### اضافه کردن کد جاوا اسکریپت:
```html 
    ```javascript
        //code
    ```
```

```mermaid
%%{init: {'theme': 'base', 'themeVariables': {'textColor': 'lightgrey', 'lineColor': 'lightgrey'}}}%%
graph LR
    A[Start] --> B[process]
    B --> C{if}
    C --> |true| D[process 2]
    C --> |false| B
    D --> E{if}
    E --> F(result 1)
    E --> G(result 2)
```
مثالی برای کاربرد [ **DNS** ](https://en.wikipedia.org/wiki/Domain_Name_System)

```mermaid
sequenceDiagram
    rect lightgrey
    participant A as User
    participant B as DNS
    participant D as DNS 2
    participant C as google.com Server

    A ->> B : Whats the IP address of google.com?
    activate B
    activate B
    alt I have it
    B --) A : IP address is "142.250.179.174"
    deactivate B
    end
    alt I have not 
    B ->> D : have U IP address of google.com?
    activate D
    D --) B : IP address is "142.250.179.174"
    deactivate D
    note over B : cache: google.com = "142.250.179.174"
    B --) A : IP address is "142.250.179.174"
    deactivate B
    end
    note left of A :cache: google.com = 142.250.179.174

    A ->> C : get me "x" file
    activate C
    C --) A : "x" file
    deactivate C
    end
```



[gitHub]: https://github.com/ErfanSafaei78
[taban-e shahr]: https://tabaneshahr.com/
